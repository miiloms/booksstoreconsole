##Консольное приложение для библиотеки
***
###Запуск
* Запустить IDE.
###Описание
* Консольное приложение для библиотеки.
* Написана на С++.
* Функционал: добавление книг,  вывод списка книг, получить книгу на руки, вернуть книгу, удалить книгу, редактировать данные книги, сохранение в файл на диске.
* Для хранения информации о книгах применялся двусвязный список.
* Запись на диск производится в бинарный файл на диск C:/.
***

###Скриншоты программы
####Главное меню
![Screenshot_1.png](https://bitbucket.org/repo/EKAbdk/images/3703781914-Screenshot_1.png)
####Вывести список книг
![Screenshot_2.png](https://bitbucket.org/repo/EKAbdk/images/2166985528-Screenshot_2.png)
####Взять книгу, чтобы отредактировать данные о ней
![Screenshot_3.png](https://bitbucket.org/repo/EKAbdk/images/482194763-Screenshot_3.png)
#### 
![Screenshot_4.png](https://bitbucket.org/repo/EKAbdk/images/3817604147-Screenshot_4.png)
####Возврат книги
![Screenshot_5.png](https://bitbucket.org/repo/EKAbdk/images/189918992-Screenshot_5.png)
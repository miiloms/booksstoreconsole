#include <iostream>
#include<ctime>
#include <conio.h>
#include <string>
#include <iomanip>
#include <ctype.h>
using namespace std;
struct book_list //��������� ��� �������� ������
{
	char num_UDC[10];
	char sec_name[30];
	char b_name[40];
	int year;
	int count;
	book_list* next;  //��������� �� ������� ������ �������
	book_list* prev;  //��������� �� ���������� �������
	book_list* first; //��������� �� ��������� ������� � ������
	book_list() :next(NULL), prev(NULL), first(NULL)  //���������� �� ��������� � �������������� ����������
	{}
	book_list(char* num, char* sn, char* bn, int y, int c)//����������� � �����������
	{                                                     //(��� ������������� ���������� ���������)
		strcpy(num_UDC, num);
		strcpy(sec_name, sn);
		strcpy(b_name, bn);
		year = y;
		count = c;
		next = NULL;
		prev = NULL;
	}
};
void add(book_list*);                 //������� ���������� ������
void show_all(book_list*);            //������� �������� ��� ������
void get_item(book_list*, char*);     //������� ����� � ����� �����
void take_book(book_list*);           //������� ����� ����� �����
void edit_book(book_list*);           //������� �������������� �����
void del_book(book_list*, book_list*);//������� �������� �����
void put_book(book_list*);            //������� ������� �����
void save_list(book_list*);           //������� ��������� ������ �� ����
void disp_row(book_list*, bool);      //������� �������� ���� ������� ��� ��� �������:(1)-���, (0)-����
bool pass(char*);                     //�������� ������
//////////////////////////////////////////////////////////
int main()
{

	char ans;        //��������� ��� ������ �������
	char udc[10];    //���������� ��� ����� ��� �����
	book_list books; //����������� ������� ���������
	unsigned int size_file, size_structr = sizeof(books); //������ ����� ��������� ��� ������ � �����
	FILE* file_b;
	cout << "Opening file [C://library.dat]...";
	file_b = fopen("C://library.dat", "a+b");             //��������� ���� ������
	fseek(file_b, 0, SEEK_END);                           //������������� ���������� �� ����� �����
	size_file = ftell(file_b);                            //���������� ������ �����
	if (!file_b)                                          //���� ������ � �������� �����->�������
	{
		perror("Error opening file:");
		cin.get(); exit(1);
	}
	if (size_file && !(size_file % size_structr))         //���� ���� ������ ��� ���� ���������->�������� ��� �����
	{
		fseek(file_b, 0, SEEK_SET);                       //������������ �� ������ ������
		book_list* list_file;                             //��������� �� ���������
		while (1)
		{
			list_file = new book_list;                    //�������� ������ ��� ����� ���������
			fread(list_file, size_structr, 1, file_b);    //������ � ��� ������ �� ��������� ������ 
			if (feof(file_b))                             //���� ����� ������- ������� �� �����
				break;
			list_file->prev = books.first;                //� ���� prev ������ �������� ���������� ��������� �� ���������� �������
			if (books.first)                              //���� ��� �� ������ ������(�� NULL), 
				books.first->next = list_file;            //�� � ���� next ���������� �������� ���������� ��������� �� ����� �������
			books.first = list_file;                      //�������(�������) ��������� ���������� ��� ����� �������
			list_file->next = NULL;                       //��������(��������) �������� ���������� ������� ���������
		};
		fclose(file_b);                                   //��������� ����
		cout << endl << "There were found " << size_file / size_structr << " books in file";
	}
	else
	{
		cout << endl << "Data from file aren't available. Working in create-mode!";
		fclose(file_b);                                   //��������� ���� ���� 
	}
	do                                                    //����������� ���� ��� ����
	{
		cout << endl << "___________________" << endl;
		cout << "Head menu:" << setw(10) << "|" << endl << "(A)dd_a_book" << setw(8) << "|" << endl << "(D)isplay_all_books" << setw(1) << "|" << endl
			<< "(G)et_a_book" << setw(8) << "|" << endl << "(P)ut_a_book" << setw(8) << "|" << endl << "(S)ave_list" << setw(9) << "|" << endl << "(E)xit" << setw(14) << "|";
		cout << endl << "___________________|" << endl;
		ans = getch();
		switch (ans)                                      //����. ������������ ������ �������  
		{                                                 //� ����������� �� ��������� �������
		case 'a': add(&books); break;
		case 'd': show_all(&books); break;
		case 'g': if (!books.first)                      //���� ��� ����-> ������� ���������
		{
					  cout << endl << "Books are not available! Please, add information if you need..."; break;
		}
				  cout << endl << "Please, enter number of UDC: "; fflush(stdin); cin.get(udc, 10);
				  get_item(&books, udc); break;
		case 'p':put_book(&books); break;
		case 's': save_list(&books); break;
		case 'e': cout << endl << "Are you sure?(Y/N)";
			fflush(stdin);
			if (getch() == 'y')
				exit(1);
			break;
		default: cout << endl << "Wrong command! Please, try again:"; break;
		}
	} while (1);
	system("PAUSE");
	return 0;
}
//////////////////////////////////////////////////////////
void get_item(book_list* list, char* udc)                 //������� ���������� ������ ����� �� ����, � ������� ���� ��� �����
{
	book_list* current = list->first;                     //������ ��������� �� ������� �������

	while (current->prev)                                 //���� ���� �� ������ � ������ ������(prev=NULL)
	{
		if (!strcmp(current->num_UDC, udc))               //��� ���������� �������� ������-������� �� �����
			break;
		current = current->prev;                          //����� ������ ��������� � ������ ������
	};
	if (!current->prev && strcmp(current->num_UDC, udc))  //���� ��� ��� ��������� ������� � �����(�����) �� ��������� -> 
	{                                                     //�� ������� �� ������� � ����������
		cout << endl << "The book is not available!";
	}
	else                                                  //����� (������� ������) ��������� ���� ��� �����
	{
		char ans2;
		do
		{
			disp_row(current, 0);                           //���������� ��������� �����
			cout << "_____________________" << endl;
			cout << "Book menu:" << setw(12) << "|" << endl << "(D)elete_the_book" << setw(5) << "|" << endl << "(E)dit_the_book" << setw(7) << "|" << endl
				<< "(T)ake_the_book" << setw(7) << "|" << endl << "(G)o_to_the_Head_menu" << setw(1) << "|";
			cout << endl << "_____________________|" << endl;
			fflush(stdin);
			ans2 = getch();
			switch (ans2)
			{
			case 'd':cout << endl << "Are you sure that you want to delete (" << current->b_name << ")?(Y/N)" << endl;
				if (getch() == 'y')
				{
					cout << "The book" << "'" << current->b_name << "'" << "was deleted successfully!" << endl; del_book(current, list); break;
				}
				break;
			case 'e':cout << endl << "Enter password: ";
				char p[10];
				fflush(stdin); cin.get(p, 10);
				if (pass(p))
				{
					cout << "Loggin complete!" << endl; edit_book(current); break;
				}
				else{ cout << endl << "Wrong password!"; break; };
			case 't':take_book(current); break;
			case 'g':break;
			default: cout << endl << "Wrong command! Please, try again:"; break;
			}
		} while (!(ans2 == 'g') && !(ans2 == 'd'));           //����, ���� �� ����� ������� ����� � �� ���� 
	}                                                     //��� ����� ����� �������
}
void take_book(book_list* current)                        //������� ������ ����� �����
{
	if (current->count == 0)                              //���� ������� ����� ���� - �� ��������� �� ���������� ����
	{
		cout << endl << "All the books are  on  the hands!" << endl;
	}
	else
	{
		cout << endl << "The book is available! You may to take the book(" << current->b_name << ")." << endl << " Number of books: " << current->count << endl;
		current->count--;                                 //��������� ������� ����� ��� � �������
	}

}
void del_book(book_list* current, book_list* books_f)     //������� �������� �������� �� ������
{
	if (current->next&&current->prev)                     //���� ������� ��������� � �������� ������
	{
		current->next->prev = current->prev;              //��������� �������� ������ � ����� �� ����������
		current->prev->next = current->next;
		delete current;                                   //����������� ������ �� ����� ��������
	}
	else
	{
		if (!current->prev&&!current->next)               //���� ������� ���� � ������
		{
			books_f->first = NULL;                        //��������� �� �������� ������� ��������
			delete current;
		}
		else
		{
			if (!current->prev)                           //���� ������� � ������ ������
			{
				current->next->prev = NULL;               //��������� prev ���������� �� ��������� ��������� ��������
				delete current;
			}
			else                                          //���� ������� ��������
			{
				current->next = NULL;
				books_f->first = current->prev;
				delete current;
			}
		}
	}
}
void edit_book(book_list* current)                        //������� �������������� �����
{
	int ans3, y, c;                                       //���������� ��� ����� ����� ������
	char num[10], sn[30], bn[40];
	do
	{
		cout << endl << "Please, choose number of row which you want to edit:";
		cout << endl << "1)number of UDC: " << current->num_UDC;
		cout << endl << "2)second name & INI of author: " << current->sec_name;
		cout << endl << "3)book's name: " << current->b_name;
		cout << endl << "4)years of bublishing: " << current->year;
		cout << endl << "5)number of books: " << current->count << endl;
		fflush(stdin);
		ans3 = getch();
		switch (ans3)
		{
		case '1': cout << endl << "Editing of row No1: ";
			fflush(stdin); cin.get(num, 10);
			strcpy(current->num_UDC, num);
			cout << "Edit complete!" << endl; break;
		case '2': cout << endl << "Editing of row No2: ";
			fflush(stdin); cin.get(sn, 30);
			strcpy(current->sec_name, sn);
			cout << "Edit complete!" << endl; break;
		case '3': cout << endl << "Editing of row No3: ";
			fflush(stdin); cin.get(bn, 40);
			strcpy(current->b_name, bn);
			cout << "Edit complete!" << endl; break;
		case '4': cout << endl << "Editing of row No4: ";
			fflush(stdin); cin >> current->year;
			cout << "Edit complete!" << endl; break;
		case '5': cout << endl << "Editing of row No5: ";
			fflush(stdin); cin >> current->count;
			cout << "Edit complete!" << endl; break;
		default: cout << endl << "Wrong command! Please, try again:"; break;
		}
		cout << endl << "Do you want to edit again?(Y/N)";
		fflush(stdin);
	} while (!(getch() == 'n'));
}
void add(book_list* list)                                 //������� ���������� ����� ������   
{
	char num[10], sn[30], bn[40];                         //���������� ��� ����� ��������� ������
	int y, c;
	cout << endl << "Please, enter information about book" << endl
		<< "number of UDC: "; fflush(stdin); cin.get(num, 10);
	cout << "second name & INI of author: "; fflush(stdin); cin.get(sn, 30);
	cout << "book's name: "; fflush(stdin); cin.get(bn, 40);
	cout << "years of bublishing: "; fflush(stdin); cin >> y;
	cout << "number of books: "; fflush(stdin); cin >> c;
	book_list* book = new book_list(num, sn, bn, y, c);   //�������� ������ ��� ���������� ��������� � �����������
	book->prev = list->first;                             //������ �������� � ���� prev ������� ����� ��� �����������
	book->next = NULL;                                    //� �������� ���� next
	if (list->first)                                      //���� ������ �� ������, �� � ���� next ��� ����������� ��������
		list->first->next = book;                         //���������� ��������� �� ����� �������
	list->first = book;                                   //� � ������� ��������� ���������� ����������� ������ ��.
}
void show_all(book_list* book)                            //������� �������� ��� �����
{
	book_list* current = book->first;                     //�������������� ��������� ��������� ���������
	if (current)                                          //����  NULL �� ������� ��������� �� ��������� ������
	{
		while (current->prev)                             //��������� �� ������ ������
		{
			current = current->prev;
		};
		disp_row(current, 1);                              //������� ������� (1-���)
	}
	else
	{
		cout << endl << "Books are not available! Please, add information if you need...";
	}
}
void disp_row(book_list* current, bool b)
{
	cout << endl << "===============================================================================";
	cout << endl << setw(10) << "No of UDC" << setw(2) << "|" << setw(12) << "Sname & INI" << setw(5) << "|" <<
		setw(12) << "Book's name" << setw(10) << "|" << setw(9) << "Year of publ." << "|" << setw(12) << "Numb of books";
	cout << endl << "===============================================================================" << endl;
	do                                                //� ������ �� ����� ������� �������� �� �����
	{
		cout.setf(ios::left); cout << setw(11) << current->num_UDC << "|"; cout.unsetf(ios::left);
		cout << setw(16); cout.setf(ios::left); cout << current->sec_name << "|"; cout.unsetf(ios::left);
		cout << setw(21); cout.setf(ios::left); cout << current->b_name << "|"; cout.unsetf(ios::left);
		cout << setw(13); cout.setf(ios::left); cout << current->year << "|"; cout.unsetf(ios::left);
		cout << setw(12); cout.setf(ios::left); cout << current->count << endl; cout.unsetf(ios::left);
		cout << "-------------------------------------------------------------------------------" << endl;
		current = current->next;
	} while (current&&b);                                 //� ����������� �� [b] ������� ��� ��� ���� ������
}
void put_book(book_list* list)                            //������� ������� ����� �������
{
	book_list* current = list->first;
	if (!current)                                         //���� ��� � ���� ����->������� �� �������
	{
		cout << endl << "The book is not available!";
		return;
	}
	char udc[10];
	cout << "Please, enter UDC of book for return: ";
	fflush(stdin); cin.get(udc, 10);                       //��������� ����� ��� �� ������������
	while (current->prev)                                 //���� ����������, �������� ������ �� ���� �� ������
	{                                                     //�� ������� ��������
		if (!strcmp(current->num_UDC, udc))
			break;
		current = current->prev;
	};
	if (!current->prev && strcmp(current->num_UDC, udc))  //���� ������ ������� ���� �� ���������, �� �������
	{
		cout << endl << "The book is not available!";
	}
	else                                                  //����  ������ � ����� �������, 
	{                                                     //�� ����������� �������
		current->count++;
		cout << endl << "The book '" << current->b_name << "' was returned successfully!";
	}
}
void save_list(book_list* list)                           //������� ���������� ������ �� ����
{
	book_list* current = list->first;                     //�������������� ��������� ��������� � ������ ���������
	if (current)                                          //���� ���� ������, ��������� ���� �� ������
	{
		cout << endl << "Data saving...";
		FILE* file_b;
		file_b = fopen("C://library.dat", "wb");          //�������������� ����, ���� ���, ������� �����
		if (!file_b)                                       //�������� �� ������ ������ � ������
		{
			perror("Information can't be save!:"); return;
		}
		unsigned int size_structr = sizeof(*current);     //������� ������ ���������
		fseek(file_b, 0, SEEK_SET);                       //������ �� ������ ������
		fwrite(current, size_structr, 1, file_b);         //����� � ����� ������ ������
		while (current->prev)                             //��� ���� ����� �� ��� � �����, ���� >1 ��������
		{
			current = current->prev;
			fwrite(current, size_structr, 1, file_b);
		};

		fclose(file_b);                                   //��������� ����
		cout << endl << "Data was saved successfully at the DISC [C://library.dat]";
	}
	else                                                  //����� ��� ������ ��� ����������
	{
		cout << endl << "No data for saving!";
	}
}
bool pass(char* password)
{
	return !strcmp(password, "12345");
}